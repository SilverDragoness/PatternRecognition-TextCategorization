# This script is responsible for unitesting the function of the scripts.

import unittest




if __name__ == '__main__':
    unittest.main()
    


'''
class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)




lis = wordDictionaryLinkedList()
lis.updateWordListWithWord("ωρα")
lis.updateWordListWithWord("γατα")
lis.updateWordListWithWord("αρκουδα")
lis.updateWordListWithWord("βατραχος")
lis.updateWordListWithWord("ψιχουλα")
lis.updateWordListWithWord("αετος")
lis.updateWordListWithWord("αετος")
lis.updateWordListWithWord("ψωμι")
lis.updateWordListWithWord("βαρκα")
lis.updateWordListWithWord("γαιδαρος")
lis.updateWordListWithWord("γουρουνι")
lis.updateWordListWithWord("γω")
lis.updateWordListWithWord("ωκεανος")
lis.updateWordListWithWord("ωμεγα")
lis.updateWordListWithWord("αβ")
lis.updateWordListWithWord("ωρα")
lis.updateWordListWithWord("γατα")
lis.updateWordListWithWord("αρκουδα")
lis.updateWordListWithWord("βατραχος")
lis.updateWordListWithWord("ψιχουλα")
lis.updateWordListWithWord("αετος")
lis.updateWordListWithWord("αετος")
lis.updateWordListWithWord("ψωμι")
lis.updateWordListWithWord("βαρκα")
lis.updateWordListWithWord("γαιδαρος")
lis.updateWordListWithWord("γουρουνι")
lis.updateWordListWithWord("γω")
lis.updateWordListWithWord("ωκεανος")
lis.updateWordListWithWord("ωμεγα")
lis.updateWordListWithWord("αβ")
lis.updateWordListWithWord("γατα")
lis.updateWordListWithWord("αρκουδα")
lis.updateWordListWithWord("βατραχος")
lis.updateWordListWithWord("ψιχουλα")
lis.updateWordListWithWord("αετος")
lis.updateWordListWithWord("αετος")
lis.updateWordListWithWord("ψωμι")
lis.updateWordListWithWord("βαρκα")
lis.updateWordListWithWord("γαιδαρος")
lis.updateWordListWithWord("γουρουνι")
lis.printList()

lis = sortedNumericallyLinkedList()
lis.updateListWithEntry("hi",10)
lis.updateListWithEntry("ho",1)
lis.updateListWithEntry("hue",5)
lis.updateListWithEntry("he",15)
lis.updateListWithEntry("h",15)

lis.updateListWithEntry("hi",10)
lis.updateListWithEntry("ho",1)
lis.updateListWithEntry("hue",5)
lis.updateListWithEntry("he",15)
lis.updateListWithEntry("hp",7)
lis.updateListWithEntry("q",7)
lis.updateListWithEntry("s",7)
lis.updateListWithEntry("r",7)
lis.updateListWithEntry("hi1",10)
lis.updateListWithEntry("ho2",1)
lis.updateListWithEntry("hue3",5)
lis.updateListWithEntry("he4",15)
lis.updateListWithEntry("h5",15)
lis.updateListWithEntry("hi6",10)
lis.updateListWithEntry("ho7",1)
lis.updateListWithEntry("hue8",5)
lis.updateListWithEntry("he9",15)
lis.updateListWithEntry("hp10",7)
lis.updateListWithEntry("q11",7)
lis.updateListWithEntry("s12",7)
lis.updateListWithEntry("r1",7)

lis.printList()
'''