# This script is responsible for the preprocessing of the raw data.
# It removes special characters such as ' or  as well as numbers and also groups specific words? # need to think about that a bit more.
# All lower case will also help with the special character removal.
# Also a good idea would be to remove articles.
# Do we really want to read and again write in a file?
# When does a capital letter considered a name?
# I need to think of exluding english words

import sys
import codecs
import re
import nltk

import globalVariables
import utilities

# This line is needed when ever I want to print greek text from the raw files for debugging purposes.
#sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

testingString = "Ο όρος multi culti δεν αφορά φυσικά μόνον τη σύγχρονη γαστρονομία, αφορά κάθε έκφανση του πολιτισμού και εν προκειμένω περιγράφει εύστοχα το Φεστιβάλ «Ωδές» που θα γίνει από τις 5 έως τις 7 Μαΐου στο Διογένης Στούντιο και στο Απόλλων (Λ. Συγγρού και Αμφιθέας) 22."
testingFileLetsSay = testingString.split(" ")

# Data structure containing the special characters we need to remove.

specialCharacters = {',', '.', '(', ')', '«', '»', '\''} # we are going to handle them differenently.
accentedVowels = {'ά':'α', 'έ':'ε', 'ί':'ι', 'ΐ':'ι', 'ϊ':'ι', 'ή':'η', 'ό':'ο', 'ύ':'υ', 'ϋ':'υ', 'ώ':'ω'}
adverbs = {'που', 'που', 'πουθε', 'ποτε', 'πως', 'ποσο', 'ναι', 'μαλιστα', 'βεβαια', 'βεβαιοτατα', 'ορισμενως', 'αληθεια', 'αληθινα', 'σωστα', 'ισως', 'ταχα', 'ταχατε', 'δηθεν', 'πιθανον', 'αραγε', 'οχι', 'δε', 'δεν', 'μη', 'μην', 'πουθενα', 'αλλου', 'καποτε', 'καπου', 'ποτε', 'αλλοτε', 'καπως', 'αλλιως', 'αλλιωτικα', 'καμποσο', 'εδω', 'κει', 'αυτου', 'παντου', 'τοτε', 'τωρα', 'ποτε', 'ετσι', 'μαζι', 'τοσο', 'εκει', 'που', 'οπου', 'οπουδηποτε', 'οποτε', 'οποτεδηποτε', 'οπως', 'οπως','καθως', 'ως', 'σαν', 'οσο', 'οσοδηποτε', 'επανω', 'πανω', 'κατω ', 'καταγης', 'μεσα', 'εξω', 'εμπρος', 
           'μπρος', 'πισω', 'δεξια', 'αριστερα', 'ψηλα', 'χαμηλα', 'πλαι', 'διπλα', 'κοντα', 'μακρια', 'απεναντι', 'γυρω', 'ολογυρα', 'μεταξυ', 'αναμεταξυ', 'αναμεσα', 'περα', 'αντιπερα', 'βορεια', 'νοτια', 'ανατολικα', 'δυτικα', 'βορειοανατολικα', 'νοτιοανατολικα', 'αμεσως', 'ευθυς', 'κιολας', 'ηδη', 'πια', 'μολις', 'ακομη', 'ακομα', 'παλι', 'ξανα', 'σνχνα', 'συνηθως', 'υστερα', 'επειτα', 'κατοπιν', 'πρωτα', 'πριν', 'πρωτυτερα', 'νωρις', 'αργα', 'γρηγορα', 'συναμα', 'αδιακοπα', 'εγκαιρα', 'χτες', 'προχτες', 'σημερα', 'εψες', 'ψες', 'αποψε', 'αυριο', 'μεθαυριο', 'περσι', 
           'περυσι', 'προπερσι', 'φετος', 'καλα', 'κακα', 'σιγα', 'εξαφνα', 'αξαφνα', 'ξαφνικα', 'ισια', 'ωραια', 'χωριστα', 'μονο', 'μοναχα', 'μοναχα', 'διαρκως', 'μεμιας', 'μονομιας', 'επισης', 'επικεφαλης', 'ιδιως', 'κυριως', 'ειδεμη', 'κακου', 'τυχον', 'καλως', 'ακριβως', 'εντελως', 'ευτυχως', 'εξης', 'κτλ', 'μονο', 'πολυ', 'περισσοτερο', 'πιο', 'λιγο', 'λιγακι', 'λιγοτερο', 'κομματι', 'αρκετα', 'σχεδον', 'τουλαχιστο', 'τουλαχιστον', 'πανω κατω', 'περιπου', 'καθολου', 'διολου', 'ολωσδιολου', 'ολοτελα', 'μαλλον', 'εξισου', 'μολις'
           }
prepositionsAndMore = {'με', 'σε', 'για', 'ως', 'πριν', 'προς', 'σαν', 'αντι', 'απο', 'διχως', 'εως', 'κατα', 'μετα', 'μεχρι', 'παρα', 'χωρις', 'εναντιον', 'εξαιτιας', 'ισαμε', 'μεταξυ', 'οχι', 'ναι', 'θα', 'εν', 'εκ', 'ας', 'κι', 'να', 'ειμαι', 'ημουν', 'οντας', 'εισαι', 'ησουν', 'ειναι', 'ηταν', 'ειμαστε', 'ημαστε', 'ημασταν', 'ειστε', 'εισαστε', 'ησαστε', 'ησασταν'}

# Instead of using a data structure for the articles maybe I should work with the length of the words. 
# There are articles from the range of 1 to 4 letters while there are 4 letter usefull words and some 2 letter ones (γη) 
articles = {'το', 'οι', 'τα', 'του', 'της', 'των', 'τον', 'την', 'τη', 'τους', 'τις', 'στο', 'στα', 'στη', 'στην', 'στον', 'στους', 'στις' ,'στων'}

conjunctionsAndConnecting = {'και', 'κι', 'ουτε', 'μητε' ,'ουδε', 'μηδε', 'η', 'ειτε', 'αλλα', 'μα', 'παρα', 'ομως', 'ωστοσο', 'ενω', 'μολονοτι', 'μονο', 'λοιπον', 'ωστε', 'αρα', 'επομενως', 'οποτε', 'δηλαδη', 'πως', 'που', 'οτι', 'οταν', 'ενω', 'καθως', 'αφου', 'αφοτου', 'πριν', 'μολις', 'προτου', 'ωσπου', 'ωσοτου', 'γιατι', 'επειδη', 'αφου', 'τι', 'αν', 'εαν', 'αμα', 'να', 'για', 'ωστε', 'που', 'ενω', 'μολονοτι', 'μην', 'μη', 'μηπως', 'παρα', 'να'}

pronouns = {'εγω', 'εσυ', 'εμενα', 'εσενα', 'εμενα', 'με', 'εσενα', 'σε', 'εμεις', 'εσεις', 'εμας', 'μας', 'εσας', 'σας', 'αυτος', 'αυτης', 'αυτου', 'αυτον', 'αυτη', 'αυτην', 'αυτο', 'αυτοι', 'αυτων', 'τους', 'αυτους', 'αυτες', 'αυτα', 'δικο', 'δικος', 'δικη', 'σου', 'εαυτου', 'μου', 'εαυτο ', 'εαυτων ', 'ιδιος', 'ιδια', 'ιδιο', 'μονος', 'μονη', 'μονο', 'αυτος', 'αυτη', 'αυτο', 'τουτος', 'τουτη', 'τουτο', 'ετουτος', 'ετουτη', 'ετουτο', 'εκεινος', 'εκεινη', 'εκεινο', 'τετοιος', 'τετοια', 'τετοιο', 'τοσος', 'τοση', 'τοσο', 'οποιος', 'οποια', 'οποιο', 'οποιος', 'οποια', 'οποιο', 'ο,τι', 'οσος', 'οση', 'οσο', 'ποιος', 'ποια', 'ποιο', 'ποσος', 'ποση', 'ποσο', 'ενας', 'μια', 'μια', 'ενα', 'κανενας ', 'κανεις', 'καμια', 'καμια', 
            'κανενα', 'καποιος', 'καποια', 'καποιο', 'μερικοι', 'μερικες', 'μερικα', 'κατι', 'κατιτι', 'τιποτε', 'τιποτα', 'καμποσος', 'καμποση', 'καμποσο', 'καθε', 'καθενας', 'καθεμια', 'καθεμια', 'καθενα', 'ποιος', 'ενας', 'καποιος', 'αυτος', 'κανενας', 'μερικοι', 'τουτος', 'ετουτος', 'οποιος', 'αλλος', 'εκεινος	', 'δεινα', 'ταδε', 'καθενας', 'κατι', 'κατιτι', 'τουτο', 'ο,τι', 'καθε', 'καθετι', 'εκεινο', 'οποιοσδηποτε', 'τετοιος', 'οτιδηποτε', 'ποσος', 'καμποσος', 'τοσος', 'οσος'}

# I can be checking if there was a start of the sentance. A dot in the previous word. If so the next word is always capital and almost never 
# a actuall name. So it can be turn to lower case without much thought and after remove special characters or delete in case of an article.
# if there was no starting of sentance then its a name and we must treat it differently.
# The dictionaries I want to keep are each reffering to a chategory.
# Also we need to determine the chategory from the name of the file as it seems.

# Handler functions.

# First stage handler

def handle_one_syllable_words(word):
    
    utilities.preProc_feedback_print("-= First stage:") 
    utilities.preProc_feedback_print("-= The check of the lenght. If the size is 1 then its not a word and it has no use. At least that what I think for now.")
    
    # Set the regex in order to digits and characters.

    numberCharRegex = re.compile(r'\d+\w*')

    if len(numberCharRegex.findall(word)) > 0:
        return None
    
    return word

def check_words_length(word):

    if len(word) == 1: # this also works for one digit numbers 
        return None 

    return word

# Second stage handler

def handle_non_greek_words(word):
    utilities.preProc_feedback_print("-= Second stage:") 
    utilities.preProc_feedback_print("Remove non greek words.")
    
    englishCharRegex = re.compile(r'[a-zA-Z]')

    if len(englishCharRegex.findall(word)) > 0:
        return None

    return word

# Third stage handler

def handle_conjunctions_connecting_words(word):

    utilities.preProc_feedback_print("-= Third stage:") 
    utilities.preProc_feedback_print("The removal of the conjunctions and connecting characters.")
    
    for doubleC in conjunctionsAndConnecting:
        if doubleC == word:
            return None

    return word

# Forth stage handler

def handle_adverbs(word):
    utilities.preProc_feedback_print("-= Forth stage:") 
    utilities.preProc_feedback_print("The removal of adverbs.")

    for adverb in adverbs:
        if adverb == word:
            return None
    return word

# Fifth stage handler

def handle_prepositions(word):
    utilities.preProc_feedback_print("-= Fifth stage:") 
    utilities.preProc_feedback_print("The removal of prepositions and more.")

    for preposition in prepositionsAndMore:
        if preposition == word:
            return None
    return word

# Sixth stage handler

def handle_special_characters(word):

    utilities.preProc_feedback_print("-= Sixth stage:") 
    utilities.preProc_feedback_print("The removal of special characters.")

    specialCharRegex = re.compile(r'\W')

    charactersFound = specialCharRegex.findall(word)

    for eachOne in charactersFound:
        word = word.replace(eachOne,"")
    
    return word

def handle_underscore(word):

    utilities.preProc_feedback_print("The removal of underscore character.")
    specialCharRegex = re.compile(r'_+')#'\d*\w*\W\w*\d*')# or number
    charactersFound = specialCharRegex.findall(word)

    for eachOne in charactersFound:
        word = word.replace(eachOne,"")   

    return word
# Seventh stage handler

def handle_numbers(word):

    utilities.preProc_feedback_print("-= Seventh stage:") 
    utilities.preProc_feedback_print("The removal of any remaining integers.")

    try:
        int(word)
        return None
    except ValueError:
        return word

# Eighth stage handler

def handle_larger_articles(word):

    utilities.preProc_feedback_print("-= Eighth stage:")
    utilities.preProc_feedback_print("The removal of larger articles.")

    for article in articles:
        if article == word:
            return None

    return word

# Nineth stage handler

def handle_pronouns(word):

    utilities.preProc_feedback_print("-= Nineth stage:") 
    utilities.preProc_feedback_print("The removal of the pronouns.")

    # I need to find where it is and replace it.
    
    for pronoun in pronouns:
        if pronoun == word:
            return None

    return word

# Tenth stage handler

def handle_accented_vowels(word):

    utilities.preProc_feedback_print("-= Tenth stage:") 
    utilities.preProc_feedback_print("The removal of the accented vowels.")

    # I need to find where it is and replace it.
    
    for accentedVowel,vowel in accentedVowels.items():
        if re.search(accentedVowel,word):
            word = word.replace(accentedVowel,vowel)

    return word

# This function removes any existing numbers

def word_preprocessing(word):
    startingWord =  word
    #pip install greek-stemmer
    #Try out a stemmer maybe? It will get faster for sure.
    utilities.preProc_feedback_print("> Running word preprocessing.")

    #startingWord = word
    # And also what do we do here? Λ. Συγγρού? Combine the name bit how
    
    word = handle_one_syllable_words(word)
    
    if word is None:
        return None

    word = check_words_length(word)

    if word is None:
        return None

    word = handle_non_greek_words(word)

    if word is None:
        return None

    # Make lower case.

    word = word.lower()

    word = handle_special_characters(word)

    if word is None:
        return None

    word = handle_accented_vowels(word)

    word = handle_conjunctions_connecting_words(word)

    if word is None:
        return None

    word = handle_adverbs(word)

    if word is None:
        return None

    word = handle_prepositions(word)

    if word is None:
        return None

    word = handle_underscore(word)

    if word is None:
        return None

    word = handle_numbers(word)

    if word is None:
        return None

    word = handle_larger_articles(word)

    if word is None:
        return None

    word = handle_pronouns(word)

    if word is None:
        return None

    if len(word) <2: 
        return None

    # Return the processed word. 

    return word 


def word_Minimum_Processing(word):

    # Make lower case.

    word = word.lower()

    word = handle_special_characters(word)

    if word is None:
        return None

    word = handle_underscore(word)

    if word is None:
        return None

    word = handle_accented_vowels(word)
    
    word = handle_numbers(word)

    if word is None:
        return None

    return word