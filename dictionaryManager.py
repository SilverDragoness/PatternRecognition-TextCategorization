# This script is responsible for the handling of the dictionaries for each category.
# There are city names as well as human names. I don't think there is a way to distinguish them. At least not yet.
# After creating a dictionary for each category we might need to trim it down. 
# dictionaries better be csv.

import os
import re

import globalVariables
import utilities
import preprocessing

# Dictionary vs set vs array vs list vs etc
# Tuples manipulation is faster than list because tuples are immutable. But we will change the number
# Good old list most likely.
# Two fields the word and a count of the times it was encountered.

artDictionaryList = utilities.sortedAlphabeticallyLinkedList()
economyDictionaryList = utilities.sortedAlphabeticallyLinkedList()
greeceDictionaryList = utilities.sortedAlphabeticallyLinkedList()
politicsDictionaryList = utilities.sortedAlphabeticallyLinkedList()
sportDictionaryList = utilities.sortedAlphabeticallyLinkedList()
worldDictionaryList = utilities.sortedAlphabeticallyLinkedList()

categories = {  'art': artDictionaryList,
                'economy': economyDictionaryList,
                'greece': greeceDictionaryList,
                'politics': politicsDictionaryList,
                'sport': sportDictionaryList,
                'world': worldDictionaryList
            }

# Maybe this needs to go to utilities.

def create_folder(directoryName, containingFolder=""):
    # Check of the directory needs to be in a containing folder.
    # If not just create it on the spot.
    if (containingFolder == ""):
        # We want to create the dictionary directory.
        # Check if the directory exists.
        if not os.path.exists(directoryName):
            os.makedirs(directoryName)
            utilities.feedback_print("Created the "+ directoryName +" file directory.")
        else:
            utilities.feedback_print("The "+ directoryName +" file directory is already created.")
    else:
        # We want to create another directory for specific data.
        if not os.path.exists(containingFolder+'/'+directoryName):
            os.makedirs(containingFolder+'/'+ directoryName)
            utilities.feedback_print("Created the " + directoryName + " directory in " + containingFolder + ".")
        else:
            utilities.feedback_print("The directory "+ directoryName + " in " + containingFolder + " is already created.")

def category_population(category):
    utilities.feedback_print("Populating the dictionary of the " + category + "category.")
    create_folder(category, globalVariables.DICTIONARY_DIR_NAME)
    
def dictionary_managment():
    #preprocessing
    #filename, file_extension = os.path.splitext(dataFile)
    category = "filename"
    category_population(category)

# Call the function that is going to pass each file from the raw data in order to populate the dictionaries for each category.
# It is directory based.

def generate_dictionaries(currentFileLocation):

    utilities.feedback_print("Generating the dictionaries.")

    create_folder(globalVariables.DICTIONARY_DIR_NAME)

    dataDirs = os.listdir(currentFileLocation)
    # TODO give another function as argument
    for directory in dataDirs:
        print(currentFileLocation+'/'+directory)
        dataForExtractionDirs = os.listdir(currentFileLocation+'/'+directory)
        
        for dataFile in dataForExtractionDirs:
            fileName, fileExtension = os.path.splitext(dataFile)
            # If its the type of file we are interested in.
            if fileExtension == '.raw':

                file = open(currentFileLocation+'/'+directory+'/'+fileName+fileExtension,'r', encoding="greek")# errors="backslashreplace")
                print("File: "+ fileName+fileExtension + '\t', end= '\r')
                try:
                    allOfTheFile = file.read()
                except UnicodeDecodeError as identifier:
                    utilities.feedback_print("There was an unicode decode error: "+str(identifier))
                    print("There are some corrupted characters in the files you supplied. Consider running sanitator first.\nExiting...")
                    exit()
                
                # We can get the category that it belongs for the traning from its name.
                category = fileName 
                correspondingCategoryKey: str = None

                # Get the appropriate list.

                for categ in categories:

                    if re.search(categ,category) is not None:
                        #_print("Found the "+categ+ " category.")
                        correspondingCategoryKey = categ
                        break
                
                if correspondingCategoryKey is None: #not categoryFound:
                    print("Can't find the corresponding dictionary. The process will terminate.")
                    exit()

                # Get the corresponding list that holds the dictionary for this category.

                correspondingCategoryList = categories[correspondingCategoryKey]

                wordList = allOfTheFile.split()
                
                for word in wordList:     

                    # Check if the word is useful or not.

                    usefulWord = preprocessing.word_preprocessing(word)

                    # Check if the node containing the word was found.

                    if usefulWord is not None:

                        correspondingCategoryList.updateWordListWithWord(usefulWord)
                    
        #printDictionaries()
        #input("Press Enter to continue...")                
    storeDictionaries(True)            

def process_dictionaries(dictionaryFolderLocation):
    # I want to access the dictionaries one at a time and process them.
    # That processing begins with arithmetical ordering and triming.

    # lets try something.
    # We will disregard the words that have a counter of 1.

    utilities.feedback_print("Processing the dictionaries.")

    dataForExtractionDirs = os.listdir(dictionaryFolderLocation)
    totalNumberOfWords = 0
    for dataFile in dataForExtractionDirs:
        print("==============================================================")
        print(dataFile)

        # Open each of the files.

        file = open(dictionaryFolderLocation + '/'+ dataFile,'r', encoding="greek")# errors="backslashreplace")
        
        numericallySortedList = utilities.sortedNumericallyLinkedList()

        for line in file:
            info = line.split(',')
            counter = int(info[1].replace(" ",""))
            if counter > 100:
                # We keep that word otherwise we ignore it
                numericallySortedList.create_and_add_Node(info[0],counter)
            
        # I can use all this in order for a first filtering of all words. they are still a lot I think.

        totalNumberOfWords += numericallySortedList.size 

def printDictionaries():
    for categ,lis in categories.items():
        print("Category: " + categ)
        lis.printList()

def storeDictionaries(alphabetically):
    utilities.feedback_print("Storing generated dictionaries...")
    utilities.feedback_print("Alphabetically? : "+str(alphabetically))

    if alphabetically:
        path = globalVariables.DICTIONARY_DIR_NAME + '/' + globalVariables.DICTIONARY_SUBDIR_NAME_AB + '/' 
    else:
        path = globalVariables.DICTIONARY_DIR_NAME + '/' + globalVariables.DICTIONARY_SUBDIR_NAME_NUM + '/' 

    for categ,lis in categories.items():
        print("Category: " + categ)
        if not os.path.exists(path + categ + ".txt"):
            wFile = open(path+categ+".txt",'w', encoding="greek")
        else:
            utilities.feedback_print("If you want the procedure to repeat for this file, delete the file.")
            exit()

        node =  lis.head
        while node:
            wFile.write(node.getWord() +', '+ str(node.getCounter())+'\n')
            node = node.nextNode
        wFile.close()    
        print("\t...Stored")

