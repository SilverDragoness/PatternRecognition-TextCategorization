import globalVariables
import utilities
import preprocessing
import sanitator

import nltk
import os
import re
import numpy
import pandas
import datetime
import sklearn
import json

from sklearn import linear_model
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA,TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB, GaussianNB
from operator import itemgetter, attrgetter, methodcaller
from sklearn.model_selection import cross_val_score

categories = {  0 : 'art',
                1 : 'economy',
                2 : 'greece',
                3 : 'politics',
                4 : 'sport',
                5 : 'world'
            }

totalNumberOfDocuments = 0

class accuracyReporter:

    max_accuracy_dictionary = {}

    def updateDictionary(self,classifier,dimention_reduction_method,case,value):
        key = classifier+'|'+dimention_reduction_method+'|'+case
        self.max_accuracy_dictionary[key] = value

# This function is being called in order to extract the features based on the word presence while returning a dictionary or list.

def extract_features_from_file_based_on_word_presence(file,freqWordList,asAList=False):

    allOfTheFile = file.read()
    documentWordList = allOfTheFile.split()
    features = {}
    presence = []
    processedDocumentWordList = []

    for word in documentWordList:     

        # Check if the word is useful or not.

        proWord = preprocessing.word_Minimum_Processing(word)

        # Check if the node containing the word was found.

        if proWord is not None:

            processedDocumentWordList.append(proWord)
             
            #correspondingCategoryList.updateWordListWithWord(proWord)
            
    for tupleW in freqWordList: 
 
        features[tupleW] = (tupleW in processedDocumentWordList)
        presence.append(int(tupleW in processedDocumentWordList))

    # Based on the type of output we want the return data structures differes.
    if asAList == True:

        # Return a list.

        return presence
    else:

        # Return a dictionary.

        return features

# This function is being called in order to trim a bit the words that correspont to the file.
# After some filtering we concluded on the words that are needed and based on these words we 
# pass over each document removing the ones that arent in the that word list we concluded.
# This make the process way faster but how much it effects accuracy is yet to be determined.

def trim_the_word_pool_per_file(file,freqWordList):

    allOfTheFile = file.read()
    documentWordList = allOfTheFile.split()
    presence = ""
    processedDocumentWordList = []

    for word in documentWordList:     

        # Do a minimum pre processing of the word.

        proWord = preprocessing.word_Minimum_Processing(word)

        # If the word after the preprocessing isnt none.

        if proWord is not None:

            processedDocumentWordList.append(proWord)
    
    for tupleW in processedDocumentWordList:

        # We do all that because we want to minimize the number of words. So we actually manage for each document to trim the words it has inside
        # That was we can actually run the script in a resonable time.
        presence = presence + (str(tupleW) + " " if (tupleW in freqWordList) else "")

    return presence

def reach_the_directory(wantedDir,currentFileLocation):
    listOfDir = os.listdir(currentFileLocation)
    for directory in listOfDir:
        if directory == wantedDir:
            dirFound = True
            return dirFound,directory
    return False,directory

# This function is going to get all the file words in a dictionary.

def extract_word_dictionary_from_documents(currentTrainingFileLocation):
    print("\nExtracting word dictionary from training data.\n")
    dataDirs = os.listdir(currentTrainingFileLocation)
    allWords = []

    for directory in dataDirs:
        utilities.feedback_print(currentTrainingFileLocation+'/'+directory) 
        dataForExtractionDirs = os.listdir(currentTrainingFileLocation+'/'+directory)
        for dataFile in dataForExtractionDirs:
            fileName, fileExtension = os.path.splitext(dataFile)

            # If its the type of file we are interested in.

            file = open(currentTrainingFileLocation+'/'+directory+'/'+fileName+fileExtension,'r', encoding="greek")# errors="backslashreplace")

            print("File: "+ dataFile + '\t', end= '\r')
            try:
                allOfTheFile = file.read()
            except UnicodeDecodeError:
                print("There are some corrupted characters in the files you supplied. Consider running sanitator first.\nExiting...")
                exit()
            
            # We can get the category that it belongs for the traning from its name.

            wordList = allOfTheFile.split()
            
            for word in wordList:     

                # Do the minimal processing in the words.

                processedWord = preprocessing.word_Minimum_Processing(word)

                # Check if the node containing the word was found.

                if processedWord is not None:

                    allWords.append(processedWord)
    
    print()
    return allWords

# This function calculates the accuracy of a multinomial and gaussian naive bayes with countervectorizer and TFIDF vectorizer.

def calculate_MNB_GNB_accuracy_CV_TFIDF(trimmedDocumentList,df_output):

    # Max acc and classifier

    x_train,x_test,y_train,y_test = train_test_split(trimmedDocumentList,df_output,test_size=0.2,random_state=4)
    cv = CountVectorizer()
    x_traincv = cv.fit_transform(x_train)
    aCv = x_traincv.toarray()

    tfidfv = TfidfVectorizer()
    x_trainTfidfv = tfidfv.fit_transform(x_train)
    aTfidfv = x_trainTfidfv.toarray()

    x_testcv = cv.transform(x_test)
    x_testTfidfv = tfidfv.transform(x_test)

    print("> Multinomial Naive Bayes with: \n")

    mnb = MultinomialNB()

    y_testArray = numpy.asarray(y_test)
    y_train = y_train.astype('int')

    mnb.fit(x_traincv,y_train)

    predictionCV = mnb.predict(x_testcv)

    calculate_accuracy('CounterVectorizer',predictionCV,y_testArray)

    mnb.fit(x_trainTfidfv,y_train)

    predictionTfidfv = mnb.predict(x_testTfidfv)

    calculate_accuracy('TfidfVectorizer',predictionTfidfv,y_testArray)

    # For Gaussian we need to use the inputs in numpy array form.

    print("\n> Gaussian Naive Bayes with: ")

    gnb = GaussianNB()

    y_testArray = numpy.asarray(y_test)
    y_train = y_train.astype('int')

    gnb.fit(aCv,y_train)

    predictionCV = gnb.predict(x_testcv.toarray())

    calculate_accuracy('CounterVectorizer',predictionCV,y_testArray)
    
    gnb.fit(aTfidfv,y_train)

    predictionTfidfv = gnb.predict(x_testTfidfv.toarray())

    calculate_accuracy('TfidfVectorizer',predictionTfidfv,y_testArray)
    
# This function calculates the accuracy of a guassian naive bayes with pca and svd based just on the presence of the words in the documents.

def calculate_GNB_accuracy_PCA_SVD_PRESENCE(df_input,df_output,cases):

    print("> Guassian Naive Bayes with presence: ")
    
    gnb = GaussianNB()
    
    print(">> PCA <<")
    print(">> Guassian Naive Bayes score with different PCA component values: ")

    for case in cases:

        pca = PCA(n_components=case, whiten='True')
        x = pca.fit(df_input).transform(df_input)

        #print(pca.explained_variance_)

        x_train, x_test, y_train, y_test = train_test_split(x,df_output,test_size=0.2, random_state=4)
        y_testArray = numpy.asarray(y_test)
        y_train = y_train.astype('int')
        gnb.fit(x_train,y_train)

        prediction = gnb.predict(x_test)

        calculate_accuracy("PCA for " + str(case) + " number of components",prediction,y_testArray)

    print("\n>> SVD <<")

    print(">> Guassian Naive Bayes score with different SVD component values: ")

    for case in cases:

        svd = TruncatedSVD(n_components = case)
        x = svd.fit(df_input).transform(df_input)

        x_train, x_test, y_train, y_test = train_test_split(x,df_output,test_size=0.2, random_state=4)
        y_testArray = numpy.asarray(y_test)
        y_train = y_train.astype('int')
        gnb.fit(x_train,y_train)

        prediction = gnb.predict(x_test)

        calculate_accuracy("SVD for " + str(case) + " number of components",prediction,y_testArray)

# This function calculates the accuracy of a guassian naive bayes with svd based a TFIDF term document matrix.

def calculate_GNB_accuracy_SVD_TFIDF(trimmedDocumentList,df_output,cases):

    print("> Combination of some of the above with GNB.")

    for case in cases:

        svd = TruncatedSVD(n_components = case)
        gnb = GaussianNB()

        tfidfv = TfidfVectorizer()
        x_trainTfidfv = tfidfv.fit_transform(trimmedDocumentList)
        aTfidfv = x_trainTfidfv.toarray()

        x_train_svd = svd.fit_transform(aTfidfv)

        x_train, x_test, y_train, y_test = train_test_split(x_train_svd,df_output,test_size=0.2, random_state=4)
        
        y_testArray = numpy.asarray(y_test)
        y_train = y_train.astype('int')

        gnb.fit(x_train,y_train)

        predictionTfidfv = gnb.predict(x_test)

        calculate_accuracy("TfidfVectorizer with SVD for " + str(case) + " number of components",predictionTfidfv,y_testArray)
        
        ###
        print("\n>>> ---------------------------------------------------------------")
        print(">>> - 5 Fold-------------------------------------------------------")
        print(">>> ---------------------------------------------------------------")
        scores = cross_val_score(gnb, x_train_svd, categArray, cv=5)

        print(">>> "+str(scores))                                            

        print(">>> Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

        # 0.8676148796498906
        print("\n>>> ---------------------------------------------------------------")
        print(">>> - 10 Fold------------------------------------------------------")
        print(">>> ---------------------------------------------------------------")
        scores = cross_val_score(gnb, x_train_svd, categArray, cv=10)

        print(">>> "+str(scores))                                            

        print(">>> Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

        # 0.8676148796498906
            
# Calculate the accuracy of a prediction based on the prediction and the test array.

def calculate_accuracy(method,prediction,testArray):
 
    count = 0

    # Check which of the prediction are actually true. Thats how we are going to check
    # the accuaracy.

    for i in range (len(prediction)):
        if prediction[i]==testArray[i]:
            count=count+1

    print("\n>> "+ method+" and accuracy: %0.3f " % (count/len(prediction)))

    return (count/len(prediction))

# This function gives some infomation about the duartion based on the starting and ending time.

def calculate_duration(startingTime):
    endingTime = datetime.datetime.now()
    print("\n======================================================")
    print("Duration: "+ str(endingTime - startingTime))
    print("======================================================\n")

def calculate_cases_based_on_the_word_range():
    wRLength = len(str(globalVariables.wordRange))
    starting = int((wRLength/3)*2)

    #cases = [int(globalVariables.wordRange/2), int(globalVariables.wordRange/4), int(globalVariables.wordRange/5), int(globalVariables.wordRange/10), int(globalVariables.wordRange/20), int(globalVariables.wordRange/40), int(globalVariables.wordRange/50), int(globalVariables.wordRange/100), int(globalVariables.wordRange/200), int(globalVariables.wordRange/500), int(globalVariables.wordRange/1000)]
    cases = [int(globalVariables.wordRange/starting), 100, 50, 25, 10, 5]
    return cases

# This function creates a documents with the trimmed list saved. 
# Writes 3 files so far in order to speed up the process. 13 mins out of 27. Half a time.
def create_needed_files(currentFileLocation):
    
    print("Creating the needed files. Approximate execution time for word range of 5000 is 13 mins...")
    currentTrainingFileLocation = currentFileLocation

    allWordsList = extract_word_dictionary_from_documents(currentTrainingFileLocation)

    numberCharRegex = re.compile(r'\w*\d+\w*')
    englishCharRegex = re.compile(r'[a-zA-Z]')

    # After extracting all the words.

    freqWordList = nltk.FreqDist(allWordsList)

    # if that number is to small there is the possibility that some documents wont have any corresponting words. That happens in case of sorted alphabetically.
    #globalVariables.wordRange = 5000 # TODO to be moved to global variables and to be added validity checks
    mostfreqWordList = []
    most_freq_Word_List_Based_on_Frequency = freqWordList.most_common() # BSOD in the 500000

    # When sorted alphabetically can actually cause a lot of issues cause it can lead to numpy.nan since the word range covers a different range of words.

    unsortedList = list(entry for entry in set(most_freq_Word_List_Based_on_Frequency) if (len(entry[0]) > 1 and entry[1] > 6 and len(numberCharRegex.findall(entry[0])) == 0 and len(englishCharRegex.findall(entry[0])) == 0))

    mostfreqWordListComplex = sorted(unsortedList, key=itemgetter(1),reverse=True) # in order to exlude words that contain integers.
    
    for entry in mostfreqWordListComplex:
        mostfreqWordList.append(entry[0])

    mostfreqWordList = mostfreqWordList[:globalVariables.wordRange]

    # Usuall loops...

    dataDirs = os.listdir(currentTrainingFileLocation)
    tempList = []
    trimmedDocumentList = []
    feature_set = []
    categList = []
    counter = 0
    for directory in dataDirs:
        dataForExtractionDirs = os.listdir(currentTrainingFileLocation+'/'+directory)
        for dataFile in dataForExtractionDirs:
            fileName, fileExtension = os.path.splitext(dataFile)
            file = open(currentTrainingFileLocation+'/'+directory+'/'+dataFile,'r', encoding="greek")# errors="backslashreplace")
            for cId,categ in categories.items():

                if re.search(categ,fileName) is not None:
                    break

            categList.append(cId) 

            tempList.append(extract_features_from_file_based_on_word_presence(file,mostfreqWordList,True))

            # Reset in the start of the file.

            file.seek(0)
            
            trimmedDocumentList.append(trim_the_word_pool_per_file(file,mostfreqWordList))
            counter +=1

    print("Number of documents: "+ str(counter))
    print("Number of words of the first document [in order to give an idea of the corpus.]: "+ str(len(tempList[0])))
    print("====================================================")

    # Start storing the lists in the appropriate files.
    
    filesN = {  'trimmedDocumentList' : trimmedDocumentList,
                'categList' : categList,
                'mostfreqWordList' : mostfreqWordList
            }
    
    for fileN,customList in filesN.items():
        print("Storing the " + fileN + " list.")
        if not os.path.exists(fileN + ".txt"):
            wFile = open(fileN+".txt",'w', encoding="greek")
        else:
            utilities.feedback_print("If you want the procedure to repeat for this file, delete the file.")
            exit()

        for l in customList:
            wFile.write(str(l) + '\n')

        wFile.close()    
        print("\t...Stored")

    print("Storing the tempList list.")
    if not os.path.exists("tempList.json"):
        with open("tempList.json", 'w') as fp:
            json.dump(tempList, fp)
    else:
        utilities.feedback_print("If you want the procedure to repeat for this file, delete the file.")
        exit()
    print("\t...Stored")

    return trimmedDocumentList,tempList,categList,mostfreqWordList

# Since in order to save time we saved some of the lists in the disk we need also to load them back in order to use them.

def load_needed_files(currentFileLocation):
    
    trimmedDocumentList = []
    tempList = []
    categList = []
    mostfreqWordList = []

    # Start loading the lists from the appropraite files.
    
    filesN = {  'trimmedDocumentList' : trimmedDocumentList,
                'categList' : categList,
                'mostfreqWordList' : mostfreqWordList
            }
    print()
    for fileN,customList in filesN.items():
        print("Loading from the " + fileN + " file.")
        if os.path.exists(fileN + ".txt"):
            file = open(fileN+".txt",'r', encoding="greek")
        else:
            utilities.feedback_print("You must first run the file creation.")
            exit()

        for line in file:
            if fileN == 'categList':
                customList.append(int(line))
            else:
                line = line.replace("\n","")
                customList.append(line)
                

        file.close()    
        print("\t...Loaded")

    print("Loading the tempList list.")
    if os.path.exists("tempList.json"):
        with open("tempList.json", 'r') as fp:
            tempList = json.load(fp)
    else:
        utilities.feedback_print("If you want the procedure to repeat for this file, delete the file.")
        exit()
    print("\t...Loaded")

    return trimmedDocumentList,tempList,categList,mostfreqWordList

# Tag the starting time of the algorithms.

startingTime = datetime.datetime.now()
midPointTime = startingTime
projectsLocation = os.getcwd()
currentFileLocation = projectsLocation

print("Searching for the raw data in order to do the training. These data are expected to be under the UncheckedFiles and enet_2000_train file located in the projects directory.")

dirFound =  False

# We take for granted that a sanitator was runned before the files.
# So the files we need are in UncorruptedFiles.

dirFound,directory = reach_the_directory(globalVariables.SRC_DIR_NAME,currentFileLocation)

if not dirFound:
    print("The train data's location cannot be found.")
    exit()

currentFileLocation = currentFileLocation+'/'+directory

# Go to that directory.

dirFound,directory = reach_the_directory('enet_2000_train',currentFileLocation)

if not dirFound:
    print("The train data's location cannot be found.")
    exit()

# Go to that directory.

currentFileLocation = currentFileLocation+'/'+directory+'/2000/'#os.getcwd()
dataDirs = os.listdir(currentFileLocation)

answer = "" 
while not ((answer == 'y') | (answer == 'n')):

    answer = input("Would you like to run a sanitator in order to remove any invalid characters? [y for Yes or n for No]\n")  

if answer == 'y':

    print("Starting file refining...")
    print("For training data.")
    for directory in dataDirs:
        dataForExtractionDirs = os.listdir(currentFileLocation+'/'+directory)
        for dataFile in dataForExtractionDirs:
            fileName, fileExtension = os.path.splitext(dataFile)
            # If its the type of file we are interested in.
            if fileExtension == '.raw':
                sanitator.file_refining(currentFileLocation+'/'+directory+'/'+fileName+fileExtension)
    trainingDataFileLocation = currentFileLocation+'/'+directory+'/'
    print("For testing data.")
    currentFileLocation = projectsLocation
    dirFound =  False
    dirFound,directory = reach_the_directory(globalVariables.SRC_DIR_NAME,currentFileLocation)

    if not dirFound:
        print("The test data's location cannot be found.")
        exit()

    currentFileLocation = currentFileLocation+'/'+directory

    # Go to that directory.

    dirFound,directory = reach_the_directory('enet_2000_test',currentFileLocation)

    if not dirFound:
        print("The test data's location cannot be found.")
        exit()

    currentFileLocation = currentFileLocation+'/'+directory+'/2000/'
    dataDirs = os.listdir(currentFileLocation)

    for directory in dataDirs:
        dataForExtractionDirs = os.listdir(currentFileLocation+'/'+directory)
        for dataFile in dataForExtractionDirs:
            fileName, fileExtension = os.path.splitext(dataFile)
            if fileExtension == '.raw':
                sanitator.file_refining(currentFileLocation+'/'+directory+'/'+fileName+fileExtension)

    testingDataFileLocation = currentFileLocation+'/'+directory+'/'                
    print("Finished file refining...")   

currentFileLocation = projectsLocation

dirFound =  False

# We take for granted that a sanitator was runned before the files. 
# So the files we need are in UncorruptedFiles.

dirFound,directory = reach_the_directory(globalVariables.DEST_DIR_NAME,currentFileLocation)

if not dirFound:
    print("The train data's location cannot be found. Please make sure the projects directory has the required subfolders.")
    exit()

currentFileLocation = currentFileLocation+'/'+directory

# Go to that directory.

dirFound,directory = reach_the_directory('enet_2000_train',currentFileLocation)

if not dirFound:
    print("The train data's location cannot be found. Please make sure the projects directory has the required subfolders.")
    exit()

# Go to that directory.
# For each folder go and get the raw data.
# Update the current file location.
currentFileLocation = currentFileLocation+'/'+directory+'/2000/'

answer = "" 
while not ((answer == 'y') | (answer == 'n')):

    answer = input("Is it the first time you run the training? If yes it is going to take some time for the lists to be populated so be patient{~13mins}. [y for Yes or n for No]\n")  

if answer == 'y':

    # Extract the required lists and also store them in the disk for feature usage in order to save time.

    trimmedDocumentList,tempList,categList,mostfreqWordList = create_needed_files(currentFileLocation)

else:

    # Load the required lists from the disk.
    
    trimmedDocumentList,tempList,categList,mostfreqWordList = load_needed_files(currentFileLocation)

# Calculate the needed dataframes arrays and lists.

trimmedDocumentArray = numpy.asarray(trimmedDocumentList)
categArray = numpy.asarray(categList)
tempArray = numpy.asarray(tempList)
df_input_presence = pandas.DataFrame(tempArray)

# Calculate the duration of the code that ran so far.

calculate_duration(midPointTime)

# Update the time stamp in order to be able to calculate the next execution duration.

midPointTime = datetime.datetime.now()

# Call the function that is going to calculate the accuracy of a multinomial naive bayes.

calculate_MNB_GNB_accuracy_CV_TFIDF(trimmedDocumentList,categArray)

# Calculate the duration of the code that ran so far.

calculate_duration(midPointTime)

# Update the time stamp in order to be able to calculate the next execution duration.

midPointTime = datetime.datetime.now()

# TODO Need to think about the range in this ones. For sure 5000 in word range is the min I think.
cases = calculate_cases_based_on_the_word_range()

calculate_GNB_accuracy_PCA_SVD_PRESENCE(df_input_presence,categArray,cases)

# Calculate the duration of the code that ran so far.

calculate_duration(midPointTime)

# Update the time stamp in order to be able to calculate the next execution duration.

midPointTime = datetime.datetime.now()

print("\n======================================================")
print("------------------------------------------------------")
print("======================================================\n")

calculate_GNB_accuracy_SVD_TFIDF(trimmedDocumentList,categArray,cases)

# Create a more special cases array
# Run 

# Calculate the duration of the code that ran so far.

calculate_duration(midPointTime)

# Update the time stamp in order to be able to calculate the next execution duration.

midPointTime = datetime.datetime.now()

print("Total program run time:", end='')
calculate_duration(startingTime)

exit()