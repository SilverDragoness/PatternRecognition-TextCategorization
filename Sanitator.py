# This script is responsible for removing the corrupted characters that exist in the data used for categorization.
# It takes the data from the UncheckedFiles directory and puts them in Uncorrupted Files after removing any corrupted characters it might encouter.
# Most likely that corrupted characters are the ' ' so we handle them by reading the file with backslashreplace error handling and after removing them manually.
# Those are in the form of �καταθλιπτικάΣ starting with the /xd2 (or xff?) byte and ending with a Σ.
import sys
import codecs
import csv
import re
import os
import shutil
import globalVariables
import utilities

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

# TODO check it the source directory is empty.
# Make it universal but first see if the one is cleared.

# This method takes as an argument the file path of the file its suppose to check for corrupted characters.

def file_refining(filePath):
    workableFilePath = os.path.relpath(filePath, str(os.getcwd()))
    utilities.feedback_print("workableFilePath = "+workableFilePath)
    create_directory()
    folderDirPath,fileName = folder_extraction_from_path(workableFilePath)
    create_directory(folderDirPath)
    
    file = open(workableFilePath,'r', encoding="greek")# errors="backslashreplace")
    try:
        file.read()
    except UnicodeDecodeError as identifier:
        utilities.feedback_print("There was an unicode decode error: "+str(identifier))
        utilities.feedback_print("Handling it...\n")
        # In this case the file contains corruption.
        file_clean_up_and_move(folderDirPath,fileName)
    else:
        # If there is no problem with the file just copy it.
        utilities.feedback_print("src = " + globalVariables.SRC_DIR_NAME+'/'+folderDirPath + '/'+fileName + " \ndst = " + globalVariables.DEST_DIR_NAME+'/'+folderDirPath +'/'+fileName )
        # Check if the file exists so we wont overwrite it.
        if not os.path.exists(globalVariables.DEST_DIR_NAME+'/'+folderDirPath +'/'+fileName): # do I want to do that?
            utilities.feedback_print(shutil.copyfile(globalVariables.SRC_DIR_NAME+'/'+folderDirPath + '/'+fileName ,globalVariables.DEST_DIR_NAME+'/'+folderDirPath + '/'+fileName ))
        else:
            print("> File " +fileName + " already exists.\n")
    
def does_dir_exist(directory):
    if not os.path.exists(directory): 
        return False
    else:
        print("> " + directory + " already exists.\n")
        return True

def folder_extraction_from_path(path):
    position = re.search('-',str(path)).span()
    folder = path[len(globalVariables.SRC_DIR_NAME)+1:position[1]+5]
    utilities.feedback_print("Folder: "+folder)
    fileName = path[position[1]+6:]
    return folder,fileName

# A function that prints the given file.
def test_the_File(path):
    rawDataFile = open(path,"r", encoding="greek")#, errors="backslashreplace")#backslashreplace surrogateescape
    allOfIt = rawDataFile.read()
    whatIreadList = allOfIt.split()
    for line in whatIreadList:
        utilities.feedback_print (line)

def create_directory(directory=""):
    if (directory == ""):
        if not globalVariables.CREATED_UNCOR_DIR:
            # We want to create the UncorruptedFiles directory.
            # Check if the directory exists.
            if not os.path.exists(globalVariables.DEST_DIR_NAME):
                os.makedirs(globalVariables.DEST_DIR_NAME)
                utilities.feedback_print("Created the "+ globalVariables.DEST_DIR_NAME +" file directory.")
                globalVariables.CREATED_UNCOR_DIR = True
            else:
                utilities.feedback_print("The "+ globalVariables.DEST_DIR_NAME +" file directory is already created.")
                globalVariables.CREATED_UNCOR_DIR = True
    else:
        # We want to create another directory for specific data.
        if not os.path.exists(globalVariables.DEST_DIR_NAME+'/'+directory):
            os.makedirs(globalVariables.DEST_DIR_NAME+'/'+ directory)
            utilities.feedback_print("Created the " + directory + " directory in " + globalVariables.DEST_DIR_NAME + ".")
        else:
            utilities.feedback_print("The directory "+ directory + " in " + globalVariables.DEST_DIR_NAME + " is already created.")

def check_if_directory_exists(directory):
    if not os.path.exists(directory):
        return False
    else:
        return True

# This function handles the corruption of the give file. It removed the corrupted characters and move the cleaned file into the a new location
# the specified folder.

def file_clean_up_and_move(folderDirPath,fileName):
    corruptedFile = open(globalVariables.SRC_DIR_NAME+'/'+folderDirPath+'/'+fileName,'r', encoding="greek", errors="backslashreplace") # maybe add try and catch
    # Check if the file itself exists.
    if does_dir_exist(globalVariables.DEST_DIR_NAME+'/'+folderDirPath+'/'+fileName):
        uncorruptedFile = open(globalVariables.DEST_DIR_NAME+'/'+folderDirPath+'/'+fileName,'w', encoding="greek")
    else:
        utilities.feedback_print("If you want the procedure to repeat for this file, delete the file.")
        return
    allOfIt = corruptedFile.read()
    whatIreadList = allOfIt.split()

    # Set the flag for the second character that is needed to be removed from the words.

    searchingForSecondPart =  False
    for line in whatIreadList:
        start = 0
        end =  len(line)
        # The corruption happens in to two parts the starting is the /xd2 (or xff?) and ends with a capital Σ which is checkable.
        if ((re.search('xd2',str(line))!=None) | (re.search('xff',str(line))!=None)):
            #foundACorruption = True
            searchingForSecondPart = True
            utilities.feedback_print("There is known corruption in the file.")
            utilities.feedback_print("========================================")
            utilities.feedback_print("Search for /xd2, found = "+ str(re.search('xd2',str(line))))
            utilities.feedback_print("Search for /xff, found = "+ str(re.search('xff',str(line))))
            start = 4
            utilities.feedback_print("========================================")
            # There is going to be an Σ as well but without a sentance after it.
            # First case
            if (line[-1]=='Σ'):
                utilities.feedback_print("FirstCase: The Σ is in the end of the same word.")
                end = end-1
                searchingForSecondPart = False

        if ((re.search('Σ',str(line))!=None) & searchingForSecondPart):    
            # Now we need to find the second part of the corruption and remove it.
            numberOfS = re.findall('Σ',str(line))

            # There can be more than one capital Σ if so you need to remove the last. Actually only 2 max?
            if (len(numberOfS) == 1):
                utilities.feedback_print("SecondCase: The Σ was found a bit later.")
                line = line.replace('Σ', "")
            else:
                # It will be at the second half of the word since we are working with line words.
                utilities.feedback_print("ThirdCase: The Σ was found a bit later but the word had multyple so we delete the last one.")
                mid = int(end/2)
                halfLine = line[mid:end].replace('Σ', "")
                line = line[start:mid] + halfLine
                end =  len(line)
            
            searchingForSecondPart = False
        #feedback_print(line[start:end])
        uncorruptedFile.write(line[start:end]+" ")
        # And also write the new file.
    utilities.feedback_print("The writing of the "+ fileName+ " at "+ globalVariables.DEST_DIR_NAME+'/'+folderDirPath+" was succesfull.")
    uncorruptedFile.close()

   