# Pattern Recognition - Text Classification of news paper articles.

This project is being implemented for the Statistical Modeling and Pattern Recognition course taken in 2018.

### Useful links

- http://ncoghlan-devs-python-notes.readthedocs.io/en/latest/python3/text_file_processing.html 
- https://docs.python.org/3.6/tutorial
- https://stackoverflow.com/
- http://thepythonguru.com/python-strings/
- http://docs.cltk.org/en/latest/greek.html
- https://www.nltk.org
- http://scikit-learn.org/stable/
- https://help.github.com/articles/basic-writing-and-formatting-syntax/
- https://people.dsv.su.se/~hercules/papers/Ntais_greek_stemmer_thesis_final.pdf

### Some information regards the implementation steps

#### textCategorization.py
- Test runs were taking hours...
- First recorded run took around ~40 mins after the proper implementation of the sorted list.
- Those runs were helpful in order to better understand my data and test the removal of corrupted characters and the preprocessing but led to an unwanted linguistic approach.

#### mainTextCategScript.py
- Initially tested some of nltk algorithms {Naive Bayes, SVD}
- After changing to mainTextCategScript ~ 30 mins
- After some better trimming and filtering with a narrower word range it went down to ~20mins {word range = 5000}
- After the list storage, successive runs can be executed at ~10 mins after the first run. {word range = 5000}

### Packet requirements

Required before running the following packages are required to be installed.
- NLTK  
- sklearn

##### NTLK

- In a windows cmd

```
pip install nltk
```

- then in a python shell we write

```
import nltk 
ntlk.download()
```

- in the ntlk downloader windows that will pop up select all and download

##### sklearn

- Similar to nltk we install sklearn:

```
pip install sklearn
```

or we have anaconda installed

```
conda install sklearn
```

### How to run

- The are two main scripts. The **textCategorization.py** and the **mainTextCategScript.py** which can be run by a terminal with the 

```
python textCategorization.py
```

or 

```
python mainTextCategScript.py
```

- The **textCategorization.py** as mentioned before was the first attempt and can give a good idea about the input data and led to the conclusion that a stemmer could be really useful but a subject of a future work.
    - It uses data structures in order to create dictionaries of the most useful words after preprocessing, for each category given.
- The **mainTextCategScript.py** is the second approach that focuses more to the implementation of:
    - Dimension reduction techniques {SVD PCA}
    - Different classifiers {Multinomial and Gaussian Naive Bayes}
    - Feature representations {Presence [1=True,0=False], CountVectorizer[counting the number of occurrences for each word], TFIDFVectorizer[calculating the TFIDF of each term]}
    - Terms can be seen in the mostfreqWordList.txt after the initial execution of the script in the project directory without their frequencies although they are sorted based on their occurrence frequency in all the corpus. 

#### Things to take into consideration

- In the **globalVariables.py** you will find and tweak variables for the better execution of the program. It is recommended that wont change anything without knowing what it does.
- You can change the flags without any problems just keep in mind that are going to effect the feedback you are receiving in the terminal window and
will slow down the scripts execution.  
- The **VariousTestResults.txt** has a lot of different test runs without all of them being with a proper implementation. They are stored as reminders.

### Feature implementation

- 1) Change into a normal logger instead of a homemade one.
- 2) Add a greek stemmer to help with the word filtering.
- 3) Try other classifiers {NNA and GMM}.
- 4) Try other dimension reduction technique {PLSA and LDA}.
- 5) Try some matrix transformations.
- 6) preprocessing: resolve what I want to do with names[How are going to handle names? For now I need everything in lower case in order for the sorting and the hash table to work.]
- 7) utilities: trim the dictionary list based on the counter.
- 8) Make it more python like [remove getters setters add properties where needed]
    - https://www.python-course.eu/python3_properties.php


*GitLab ~ https://gitlab.com/SilverDragoness/PatternRecognition-TextCategorization*
