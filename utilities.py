import os
import codecs
import sys

import globalVariables
from linkedList import ListNode
from linkedList import LinkedList

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

def create_category_folder(directoryName, containingFolder=""):
    # Check of the directory needs to be in a containing folder.
    # If not just create it on the spot.
    if (containingFolder == ""):
        # We want to create the dictionary directory.
        # Check if the directory exists.
        if not os.path.exists(directoryName):
            os.makedirs(directoryName)
            feedback_print("Created the "+ directoryName +" file directory.")
        else:
            feedback_print("The "+ directoryName +" file directory is already created.")
    else:
        # We want to create another directory for specific data.
        if not os.path.exists(containingFolder+'/'+directoryName):
            os.makedirs(containingFolder+'/'+ directoryName)
            feedback_print("Created the " + directoryName + " directory in " + containingFolder + ".")
        else:
            feedback_print("The directory "+ directoryName + " in " + containingFolder + " is already created.")

def feedback_print(string): # ,mode # end='\r')
    if globalVariables.WITH_FEEDBACK:
        print(string)

def preProc_feedback_print(string):
    if globalVariables.WITH_FEEDBACK_PREPROC:
        print(string)

def utilities_feedback_print(string):
    if globalVariables.WITH_FEEDBACK_UTIL:
        print(string)

class letterListNode(ListNode):

    # The node constructor.

    def __init__(self,letter="",correspondingNode=None,preNode=None,nextNode=None):
        super().__init__(preNode,nextNode)
        self.letter = letter
        self.correspondingNode = correspondingNode

    # Getters

    def getLetter(self):
        return self.letter

    def getCorrespondingNode(self):
        return self.correspondingNode

    # Setters 

    def setLetter(self, letter):
        self.letter = letter

    def setCorrespondingNode(self, correspondingNode):
        self.correspondingNode = correspondingNode 

class wordDictionaryListNode(ListNode):

    # The node constructor.

    def __init__(self,word="",counter=1,preNode=None,nextNode=None):
        super().__init__(preNode,nextNode)
        self.word = word
        self.counter = counter

    # Getters

    def getData(self):
        return self.word,self.counter

    def getWord(self):
        return self.word

    def getCounter(self):
        return self.counter

    # Setters 

    def setData(self,word,counter):
        self.word = word
        self.counter = counter

    def setWord(self, word):
        self.word = word

    def setCounter(self, counter):
        self.counter = counter 

    # Other node functions.

    def incrementCounter(self):
        self.counter += 1

class LetterLinkedList(LinkedList):
    def __init__(self,head:ListNode = None,tail:ListNode = None):
        super().__init__(head,tail)
        self.hashTable = {'α':None, 'β':None, 'γ':None, 'δ':None, 'ε':None, 'ζ':None, 'η':None, 'θ':None, 'ι':None, 'κ':None, 'λ':None, 'μ':None, 'ν':None, 'ξ':None, 'ο':None, 'π':None, 'ρ':None, 'σ':None, 'τ':None, 'υ':None, 'φ':None, 'χ':None, 'ψ':None, 'ω':None}
        self.create_the_letter_linkedList()

    def getLetterNode(self,key):
        letterNode = self.hashTable[key]
        return letterNode

    def getTheNextValidLetterNode(self,key):
        currentLetterNode = self.hashTable[key]
        currentLetterNode = currentLetterNode.getNextNode()
        while currentLetterNode:
            if currentLetterNode.getCorrespondingNode() is not None:
                return currentLetterNode
            currentLetterNode = currentLetterNode.getNextNode()
        return None

    def getThePreviousValidLetterNode(self,key):
        currentLetterNode = self.hashTable[key]
        currentLetterNode = currentLetterNode.getPreviousNode()
        while currentLetterNode:
            if currentLetterNode.getCorrespondingNode().getWord() is not None:
                return currentLetterNode
            currentLetterNode = currentLetterNode.getPreviousNode()
        return None

    def getWordNode(self,key):
        letterNode = self.hashTable[key]
        wordNode = letterNode.getCorrespondingNode()
        return wordNode

    def create_the_letter_linkedList(self):
        letters = ['α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ', 'ι', 'κ', 'λ', 'μ', 'ν', 'ξ', 'ο', 'π', 'ρ', 'σ', 'τ', 'υ', 'φ', 'χ', 'ψ', 'ω']

        for letter in letters:
            letterNode = letterListNode(letter,None)
            self.assineNodeToLetter(letter,letterNode)
            self.appendList(letterNode)

    # Happens once. During the init.

    def assineNodeToLetter(self,key,node):
        self.hashTable[key] = node

    def updateLetterList(self,key,wordNode):
        letterNode = self.hashTable[key]
        letterNode.setCorrespondingNode(wordNode)


    def print_the_letter_linkedList(self):
        print("Printing the letter list and the corresponing nodes.")
        node = self.head
        while node:
            print(str(node))
            print("For the letter: " + node.getLetter(), end='')
            print(" the corresponding node is " + (node.getCorrespondingNode().getWord() if (node.getCorrespondingNode() is not None) else str(node.getCorrespondingNode())))
            node = node.getNextNode()

class sortedAlphabeticallyLinkedList(LinkedList):

    # For sorting purposes. # Not sure if I need the size as well.
    # Max length of hash table should be 24 as the letters of the alphabet.

    def __init__(self,head:wordDictionaryListNode = None,tail:wordDictionaryListNode = None):
        super().__init__(head,tail)
        self.letterLinkedList = LetterLinkedList()

    def updateWordListWithWord(self, word):

        node = self.search_List_For_Word(word)
        if node is None:
            self.create_and_add_Node(word)
        else:
            node.incrementCounter()
        return node


    # Add a node based on the hash table.

    def addNode(self,newWordNode):
        # Extract the first character of the word to know the letter we are reffering to.    
        #print(newWordNode.getWord())
        startingLetter = newWordNode.getWord()[0] # must be carefull of lower case. For now I will add it all lowered case. 

        if self.size == 0:
            utilities_feedback_print("Case 0")
            # Then the first node ever added must be the head.
            self.letterLinkedList.updateLetterList(startingLetter,newWordNode)
            #self.tail = newWordNode
            self.head = newWordNode
            
        else:

            # This means thats the first word for that letter so we add it as it is in the hashTable.

            # Since there are other words we must search differently. We must see if there is a word in the word node that the letter node our word corresponds.

            if self.letterLinkedList.getWordNode(startingLetter) is None: # if the letter list says it has no word for that letter
                
                utilities_feedback_print("Case 1")

                self.letterLinkedList.updateLetterList(startingLetter,newWordNode)

                # And now we need to update the corresponding pointers of the previous and the next nodes. That is possible to happen by accessing the next existing node beloging to the next letter.

                # From that node we will get the previous as well

                # CHAOS

                #currentLetterNode = self.letterLinkedList.getLetterNode(startingLetter)
            
                nextValidAlphabeticLetterNode = self.letterLinkedList.getTheNextValidLetterNode(startingLetter)#currentLetterNode.getNextNode()

                # if the letter list says it has no word for that letter but there are words for the other letters.

                if nextValidAlphabeticLetterNode is not None:
                    
                    utilities_feedback_print("Case 1.1")

                    nextAlphabeticWordNode = nextValidAlphabeticLetterNode.getCorrespondingNode()

                    # Set the next and previous node of the new node if they exist.

                    newWordNode.setNextNode(nextAlphabeticWordNode)
                    newWordNode.setPreviousNode(nextAlphabeticWordNode.getPreviousNode())

                    # Set the previous node in order the to point to the newly added node.

                    if nextAlphabeticWordNode.getPreviousNode() is not None:

                        nextAlphabeticWordNode.getPreviousNode().setNextNode(newWordNode)

                    # Set the next Alphabetic node to point the newly added node as previous node.

                    nextAlphabeticWordNode.setPreviousNode(newWordNode)

                # There are no other nodes in the rest of the letters.

                else:

                    utilities_feedback_print("Case 1.2")

                    # We need to gain access in a different way. With the previous one. And reach the end of that letter group.
                    # It essentially means that we can add it in the end of the word list since there in nothing else after it.

                    self.tail.setNextNode(newWordNode)
                    newWordNode.setPreviousNode(self.tail)

            else:
                # When its not none it means that there more words starting with that letter.
                # String compare uses ascii values so r>c so in order to sort in an alphabetic c must be smaller than r
                # So if the starting word is larger than the given word, the given word will take its place. We shouldnt encounter any identical. Raise an error for that.

                utilities_feedback_print("Case 2")

                staringNodeForThatLetter = self.letterLinkedList.getWordNode(startingLetter)

                if staringNodeForThatLetter.getWord() > newWordNode.getWord():
                    
                    utilities_feedback_print("Case 2.1")

                    # Added in front of the starting node for that letter since we want the list to be sorted.
                    newWordNode.setNextNode(staringNodeForThatLetter)
                    newWordNode.setPreviousNode(staringNodeForThatLetter.getPreviousNode())

                    if staringNodeForThatLetter.getPreviousNode() is not None:
                        staringNodeForThatLetter.getPreviousNode().setNextNode(newWordNode)

                    staringNodeForThatLetter.setPreviousNode(newWordNode)

                    self.letterLinkedList.updateLetterList(startingLetter,newWordNode)

                elif staringNodeForThatLetter.getWord() < newWordNode.getWord():
                    # Then we must see where the appropriate place is so we pass over the part of list responsible for that letter and in the same time making sure that we dont go over to the words
                    # that have other starting letters.

                    utilities_feedback_print("Case 2.2")

                    currentSearchNode = staringNodeForThatLetter
                    placeFoundMidWay =  False
                    while currentSearchNode:
                        backUpSNode = currentSearchNode
                        if (currentSearchNode.getWord() > newWordNode.getWord()): 
                            #print("placeFoundMidWay")
                            placeFoundMidWay = True
                            break
                        currentSearchNode = currentSearchNode.getNextNode()

                    if placeFoundMidWay:

                        utilities_feedback_print("Case 2.2.1")

                        # This means that the word node we want to add must be added before the current word node.
                        # Added in front of the current node for that letter since we want the list to be sorted.

                        newWordNode.setNextNode(currentSearchNode)
                        newWordNode.setPreviousNode(currentSearchNode.getPreviousNode())
                        
                        currentSearchNode.getPreviousNode().setNextNode(newWordNode)

                        currentSearchNode.setPreviousNode(newWordNode)

                    # We are going to add in the end of that group of letters. We reached the end of the list.
                    elif currentSearchNode is None:

                        utilities_feedback_print("Case 2.2.2")

                        # Then I am going to use the back up node.

                        newWordNode.setPreviousNode(backUpSNode)

                        backUpSNode.setNextNode(newWordNode)                   
                else:
                    print("Something is wrong there shouldn't be identincal words?I think")
                    print("Case 2.3")
        
        # We need to make sure that the tail variable stays updated. Same with the head variable.
        if not (self.letterLinkedList.head.getCorrespondingNode() == self.head):
            utilities_feedback_print("    SubCase 0\n")
            self.head = newWordNode
        if newWordNode.getNextNode() is None:
            self.tail = newWordNode
            utilities_feedback_print("    SubCase 1\n")
        
        #  Increment the size of the list.

        self.size+=1

        return True

    def create_and_add_Node(self,word,counter=1):
        newWordNode = wordDictionaryListNode(word,counter)
        self.addNode(newWordNode)
        return True

    # This function searches for the input word in the linked list and returns the node it was found at.

    def search_List_For_Word(self,word):

        # Search based on the letter list.

        startingLetter = word[0]

        # In order to make the search faster we are going to search the word based on its starting letter.
        # That is possible since the word list is now sorted and I have access to a letter list and a hash table.
        
        correspondingLetterNode = self.letterLinkedList.getLetterNode(startingLetter)

        currentWordNode = correspondingLetterNode.getCorrespondingNode()

        # Now start seaching for that letter category.

        while currentWordNode:

            # Check if the word is found.
            if currentWordNode.getWord() == word:
                # The word already exists in the list.

                return currentWordNode

            # Check if we reached the end of the category or the end of the list.
            if (not (currentWordNode.getWord()[0] == startingLetter)) | (currentWordNode.getNextNode() is None):
                return None

            currentWordNode = currentWordNode.getNextNode()

    def sort_list_alphabetic(self):
        pass

    def sort_list_descenting_counter(self):
        pass

    def printList(self):
        self.letterLinkedList.print_the_letter_linkedList()
        print("The size of the list is: "+ str(self.getSize()))
        print("Printing all the list's nodes:")
        
        currentNode = self.head

        while currentNode:
            print("--------------------")
            print(currentNode.getData())
            print(currentNode.getPreviousNode())
            print(currentNode.getNextNode())
            currentNode = currentNode.getNextNode()
        print("--------------------")

class sortedNumericallyLinkedList(LinkedList):

    # For sorting purposes. # Not sure if I need the size as well.
    # Max length of hash table should be 24 as the letters of the alphabet.

    def __init__(self,head:wordDictionaryListNode = None, mid:wordDictionaryListNode = None, tail:wordDictionaryListNode = None, totalNumberOfTerms=0):
        super().__init__(head,tail)

        # In this type of linked list the head stores the word with the maximum counter and the tail the word with the minimum.
        # We also need a counter for the mid. Might actually make the search a bit faster.
        self.mid = mid
        self.totalNumberOfTerms = totalNumberOfTerms

    def updateListWithEntry(self, word, counter):

        self.create_and_add_Node(word,counter)

    # Call the function that updates the total number of terms.

    def updateTotalNumberOfTerms(self,counter):
        self.totalNumberOfTerms += counter

    # Add a node based on the hash table.

    def addNode(self,newWordNode):
        # Extract the first character of the word to know the letter we are reffering to.    

        if self.size == 0:

            self.head = newWordNode

            self.tail = newWordNode

            self.mid =  newWordNode

            #  Increment the size of the list.

            self.size+=1

        else:
            
            #  Increment the size of the list.

            self.size+=1

            # We need to find its position.
            # We will check the head and tail and after ruling out those two we will go for based on mid node
            
            if self.head.getCounter() <= newWordNode.getCounter(): # @TODO must also think the equal case.

                # In this case it will take over the head's node position.

                newWordNode.setNextNode(self.head)

                self.head.setPreviousNode(newWordNode)

                self.head = newWordNode

                if (self.size%2 == 1):
                    self.shiftMidNodeLeft()
                

            elif self.tail.getCounter() >= newWordNode.getCounter():

                # In this case it will take over the tail's node position.

                newWordNode.setPreviousNode(self.tail)

                self.tail.setNextNode(newWordNode)

                self.tail = newWordNode

                if (self.size%2 == 1):
                    self.shiftMidNodeRight()
                
            else:

                # Based on which part of the list the node was added the mid node will shift accordinally.

                if self.mid.getCounter() < newWordNode.getCounter():

                    currentNode =  self.mid

                    # Search for a posiiton for the new node.

                    while currentNode.getCounter() < newWordNode.getCounter():

                        currentNode = currentNode.getPreviousNode()

                    # The new node must be inserted after the current one.

                    newWordNode.setPreviousNode(currentNode)
                    newWordNode.setNextNode(currentNode.getNextNode())
                    
                    currentNode.getNextNode().setPreviousNode(newWordNode)
                    currentNode.setNextNode(newWordNode)

                    if (self.size%2 == 1):

                        self.shiftMidNodeLeft()
                    

                else: #self.mid.getCounter() > newWordNode.getCounter():

                    currentNode = self.mid

                    # Search for a posiiton for the new node.

                    while currentNode.getCounter() > newWordNode.getCounter():

                        currentNode = currentNode.getNextNode()

                    if currentNode.getCounter() < newWordNode.getCounter():
                        # The new node must be inserted before the current one.

                        newWordNode.setPreviousNode(currentNode.getPreviousNode())
                        newWordNode.setNextNode(currentNode)
                        
                        currentNode.getPreviousNode().setNextNode(newWordNode)
                        currentNode.setPreviousNode(newWordNode)

                    else:

                        newWordNode.setPreviousNode(currentNode)
                        newWordNode.setNextNode(currentNode.getNextNode())
                        
                        currentNode.getNextNode().setPreviousNode(newWordNode)
                        currentNode.setNextNode(newWordNode)

                    if (self.size%2 == 1):

                        self.shiftMidNodeRight()

        self.updateTotalNumberOfTerms(newWordNode.getCounter())

        return True
    
    def shiftMidNodeLeft(self):
        self.mid = self.mid.getPreviousNode()
        

    def shiftMidNodeRight(self):
        self.mid = self.mid.getNextNode()
        

    def create_and_add_Node(self,word,counter):
        newWordNode = wordDictionaryListNode(word,counter)
        self.addNode(newWordNode)
        return True

    # This function searches for the input word in the linked list and returns the node it was found at.

    def search_List_For_Word(self,word):
        pass

    def sort_list_alphabetic(self):
        pass

    def sort_list_descenting_counter(self):
        pass

    def printList(self):
        print("The size of the list is: "+ str(self.size))
        print("Printing all the list's nodes:")
        print("The word in the mid node is : " + self.mid.getWord())
        currentNode = self.head
        counter = 1
        while currentNode:

            print("--------------------")
            print(counter)
            print(currentNode.getData())
            print(currentNode.getPreviousNode())
            print(currentNode.getNextNode())
            currentNode = currentNode.getNextNode()
            counter +=1
        print("--------------------")

