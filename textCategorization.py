# Hola! I don't even remember where to start from...
# How did the raw data occured from the html file?

# So far it was found that there are some corrupted characters in the raw data their codec is 0xd2 and often followed with a capital greek S
# How are we going to work around that? it must be the ' in some language            
# Also found and SSE kind of corruption like in FrontiSSEres in art5.raw and SPA but those dont seem to cause a UnicodeDecodeError.
# We need to remove the corrupted characters somehow.
# D:\5.Static Modeling & Pattern Recognition\PatternRecognition-TextCategorization\SomeCorruption\00-05-07\art3.raw is the best for a "corraptor"

#==================
# Data Processing
#==================
# Data Extraction
# Access the files that contain the required data.

import os.path
import sys
import locale
import codecs
import urllib
import re

import sanitator
import dictionaryManager
import globalVariables
import utilities

print("getdefaultlocale : "+locale.getdefaultlocale()[1])

#sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())# cp1252 prints the files I write regardless of the extension.


def recursive_test_for_file_existance(wantedDir,directory):
    print(directory)
    if directory == wantedDir:
        return True
    listOfDir = os.listdir(currentFileLocation)
    for cDirectory in listOfDir:
        return recursive_test_for_file_existance(wantedDir,directory+'/'+cDirectory)

def reach_the_directory(wantedDir,currentFileLocation):
    listOfDir = os.listdir(currentFileLocation)
    for directory in listOfDir:
        if directory == wantedDir:
            dirFound = True
            return dirFound,directory
    return False,directory



projectsLocation = os.getcwd()
currentFileLocation = projectsLocation

#listOfDir = os.listdir(currentFileLocation)
#numberOfDirectories = len(listOfDir)

print("Searching for the raw data in order to do the training. These data are expected to be under the UncheckedFiles and enet_2000_train file located in the projects directory.")

dirFound =  False

# We take for granted that a sanitator was runned before the files. DOWE?
# So the files we need are in UncorruptedFiles.

dirFound,directory = reach_the_directory(globalVariables.SRC_DIR_NAME,currentFileLocation)

if not dirFound:
    print("The train data's location cannot be found.")
    exit()

currentFileLocation = currentFileLocation+'/'+directory

# Go to that directory.

dirFound,directory = reach_the_directory('enet_2000_train',currentFileLocation)

if not dirFound:
    print("The train data's location cannot be found.")
    exit()

# Go to that directory.
#os.chdir(currentFileLocation+'/'+directory+'/2000/') # This can also be changed and be more dynamic.
# For each folder go and get the raw data.
# Update the current file location.
currentFileLocation = currentFileLocation+'/'+directory+'/2000/'#os.getcwd()
dataDirs = os.listdir(currentFileLocation)
#numberOfDirectories = len(listOfDir)
answer = "" 
while not ((answer == 'y') | (answer == 'n')):

    answer = input("Would you like to run a sanitator in order to remove any invalid characters? [y for Yes or n for No]\n")  

if answer == 'y':

    print("Starting file refining...")
    print("For training data.")
    # TODO give another function as argument
    '''
    for directory in dataDirs:
        dataForExtractionDirs = os.listdir(currentFileLocation+'/'+directory)
        for dataFile in dataForExtractionDirs:
            fileName, fileExtension = os.path.splitext(dataFile)
            # If its the type of file we are interested in.
            if fileExtension == '.raw':
                sanitator.file_refining(currentFileLocation+'/'+directory+'/'+fileName+fileExtension)
    # TODO give another function as argument
    '''
    trainingDataFileLocation = currentFileLocation+'/'+directory+'/'
    print("For testing data.")
    currentFileLocation = projectsLocation
    dirFound =  False
    dirFound,directory = reach_the_directory(globalVariables.SRC_DIR_NAME,currentFileLocation)

    if not dirFound:
        print("The test data's location cannot be found.")
        exit()

    currentFileLocation = currentFileLocation+'/'+directory

    # Go to that directory.
    dirFound,directory = reach_the_directory('enet_2000_test',currentFileLocation)

    if not dirFound:
        print("The test data's location cannot be found.")
        exit()

    currentFileLocation = currentFileLocation+'/'+directory+'/2000/'
    dataDirs = os.listdir(currentFileLocation)

    for directory in dataDirs:
        dataForExtractionDirs = os.listdir(currentFileLocation+'/'+directory)
        for dataFile in dataForExtractionDirs:
            fileName, fileExtension = os.path.splitext(dataFile)
            if fileExtension == '.raw':
                sanitator.file_refining(currentFileLocation+'/'+directory+'/'+fileName+fileExtension)

    testingDataFileLocation = currentFileLocation+'/'+directory+'/'                
    print("Finished file refining...")   

currentFileLocation = currentFileLocation.replace(globalVariables.SRC_DIR_NAME,globalVariables.DEST_DIR_NAME)
dictionaryManager.generate_dictionaries(trainingDataFileLocation)

input("Press a key to continue...")  

dictionaryManager.process_dictionaries(trainingDataFileLocation)

  