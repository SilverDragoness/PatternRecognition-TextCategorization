class ListNode:

    # The node constructor.

    def __init__(self,previousNode=None,nextNode=None):
        self.previousNode = previousNode
        self.nextNode = nextNode

    # Getters

    def getPreviousNode(self):
        return self.previousNode

    def getNextNode(self):
        return self.nextNode

    # Setters 

    def setPreviousNode(self,previousNode):
        self.previousNode = previousNode

    def setNextNode(self,nextNode):
        self.nextNode = nextNode

class LinkedList:

    def __init__(self,head:ListNode = None,tail:ListNode = None):
        self.head = head
        self.tail = tail # current
        self.size = 0

    def getSize(self):
        return self.size

    # Add the given node to the end of the list. Not do smth based on a value cause it is being to specific

    def appendList(self,newNode):

        if self.size == 0:
            # Then the first node ever added must be the head.
            self.tail = newNode
            self.head = self.tail
        else:
            # The new node is going to be added in the end of the list.

            self.tail.setNextNode(newNode)
            newNode.setPreviousNode(self.tail)
            self.tail = newNode

        self.size+=1
        return True
