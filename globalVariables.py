# NOTE
# The training data are expected to be located in the following directory.
# \PatternRecognition-TextCategorization\UncheckedFiles
# UncheckedFiles >
#                 enet_2000_test/2000/... >
#                                            00-10-01 >
#                                                       blah.html
#                                                       blah.raw     
#                 enet_2000_train/2000/...>
#                                            00-10-02 >
#                                                       foo1.html
#                                                       foo1.raw 

# This is happening in order for the script to be able to check for corruption and inform you in case you need to run a sanitator.
# After the execution of the sanitator [more details on the readme.md] the project folder is going to be like the following with the 
# UncheckedFiles directory to remain untouched :
#\PatternRecognition-TextCategorization > 
#                                        UncheckedFiles >
#                                                         enet_2000_test/2000/...
#                                                         enet_2000_train/2000/...
#                                        UncorruptedFiles >
#                                                         enet_2000_test/2000/... >
#                                                                                   00-10-01 >
#                                                                                              blah.raw 
#                                                         enet_2000_train/2000/... >
#                                                                                   00-10-02 >
#                                                                                              foo1.raw 
# Diractory Names

SRC_DIR_NAME = "UncheckedFiles" # It is important to exits as named here at the location showed above.
DEST_DIR_NAME = "UncorruptedFiles" # Is going to be created under the given name.
DICTIONARY_DIR_NAME = "Dictionaries"
DICTIONARY_SUBDIR_NAME_AB = "SortedAlphabetically"
DICTIONARY_SUBDIR_NAME_NUM = "SortedNumerically"

# Printing flags in order to keep everything nice and clean.

WITH_FEEDBACK = True
WITH_FEEDBACK_PREPROC = False
WITH_FEEDBACK_SANIT = False
WITH_FEEDBACK_UTIL = False
CREATED_UNCOR_DIR = False

# From here you can set the word range that is going to be used for the term selection of the documents.
# Its recomented the minimum value of the wordRange to be 5000

wordRange = 5000